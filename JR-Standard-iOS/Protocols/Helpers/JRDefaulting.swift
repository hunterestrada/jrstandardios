//
//  JRDefaulting.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

public protocol JRDefaulting {
    
    // MARK: - Exposed Propeties
    
    var key: String { get }
    
}
