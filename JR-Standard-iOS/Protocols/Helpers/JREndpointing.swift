//
//  JREndpointing.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

public protocol JREndpointing {
    
    // MARK: - Exposed Properties
    
    var link: String { get }
    
    var url: URL { get }
    var baseLink: String? { get }
    
}

// MARK: - Default Implementation

extension JREndpointing {
    
    public var url: URL {
        return URL(string: link, relativeTo: baseURL) ?? NSURL() as URL
    }
    
    public var baseLink: String? {
        return nil
    }
    
    public var baseURL: URL? {
        guard let baseLink = baseLink else {
            return nil
        }
        return URL(string: baseLink)
    }
    
}
