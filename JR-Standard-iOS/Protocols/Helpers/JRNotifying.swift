//
//  JRNotifying.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

/**
 Protocol to abstract 'Notification' with default
 implementation. Conform using enum.
 */
public protocol JRNotifying {
    
    var name: String { get }
    
}

// MARK: - Default Implementation

public extension JRNotifying {
    
    var notification: Notification {
        return Notification(name: Notification.Name(rawValue: name))
    }
    
}
