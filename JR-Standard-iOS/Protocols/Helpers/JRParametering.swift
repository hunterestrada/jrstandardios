//
//  JRParametering.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

public protocol JRParametering: Hashable {
    
    var key: String { get }
    
    var value: Any { get }
    
    static func pairing(_ parameters: Self...) -> [String: Any]
    
}

// MARK: - Default Implementation

extension JRParametering {
    
    // MARK: - Exposed Properties
    
    public var value: Any {
        return key
    }
    
    public var hashValue: Int {
        return key.hashValue
    }
    
    // MARK: - Statically Exposed Properties
    
    public static func pairing(_ parameters: Self...) -> [String: Any] {
        var pairs = [String: Any]()
        parameters.forEach { parameter in
            pairs[parameter.key] = parameter.value
        }
        return pairs
    }
    
    public static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.key == rhs.key
    }
    
}
