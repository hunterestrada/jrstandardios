//
//  JRUIViewExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/29/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Exposed Functions
    
    func fadeIn(using duration: JRAnimationDuration) {
        duration.animate { [weak self] in
            guard let weak = self else { return }
            weak.alpha = 1
        }
    }
    
    func fadeOut(using duration: JRAnimationDuration) {
        duration.animate { [weak self] in
            guard let weak = self else { return }
            weak.alpha = 0
        }
    }
    
    func closest<View>(_ relatedView: RelatedView) -> View? where View: UIView {
        switch relatedView {
        case .subview:
            return UIView.closestSubview(of: self)
        case .superview:
            return UIView.closestSuperview(of: self)
        }
    }
    
    // MARK: - Statically Private Functions
    
    private static func closestSubview<View>(of view: UIView) -> View? where View: UIView {
        var pending = LinkedList(view.subviews)
        while let subview = pending.removeHead() {
            if let target = subview as? View {
                return target
            } else {
                pending.attachTail(with: subview.subviews)
            }
        }
        return nil
    }
    
    private static func closestSuperview<View>(of view: UIView) -> View? where View: UIView {
        guard let superview = view.superview else {
            return nil
        }
        if let target = superview as? View {
            return target
        }
        return closestSuperview(of: superview)
    }
    
    // MARK: - Types
    
    enum RelatedView {
        case subview, superview
    }
    
}
