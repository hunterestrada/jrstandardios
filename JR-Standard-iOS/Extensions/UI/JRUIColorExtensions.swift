//
//  JRUIColorExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/29/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

public extension UIColor {
    
    static func from(r: Int, g: Int, b: Int, a: Int = 1) -> UIColor {
        return UIColor(
            red: CGFloat(r) / maxRGB,
            green: CGFloat(g) / maxRGB,
            blue: CGFloat(b) / maxRGB,
            alpha: CGFloat(a)
        )
    }
    
    static let maxRGB: CGFloat = 255.0
    
}
