//
//  JRCollectionExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

extension Collection where Index == Int, Iterator.Element: Collection, Iterator.Element.Index == Int {
    
    // MARK: - Exposed Properties
    
    var sectionCount: Int {
        return Int(count.toIntMax())
    }
    
    // MARK: - Exposed Functions
    
    func rowCount(at section: Int) -> Int {
        guard section < sectionCount else {
            return 0
        }
        return Int(self[section].count.toIntMax())
    }
    
    // MARK: - Exposed Subscripts
    
    subscript(safe indexPath: IndexPath) -> Iterator.Element.Iterator.Element? {
        return self[safe: indexPath.section]?[safe: indexPath.row]
    }
    
}

extension Collection where Index == Int {
    
    subscript(safe index: Int) -> Iterator.Element? {
        let rawCount = Int(count.toIntMax())
        return 0 <= index && index < rawCount ? self[index] : nil
    }
    
}
