//
//  JRStringExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

public extension String {
    
    public static let empty = ""
    public static let cancel = "Cancel".localizedCapitalized
    
}
