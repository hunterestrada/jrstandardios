//
//  JRDateExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

public extension Date {
    
    // MARK: - Exposed Functions
    
    public func isAfter(_ other: Date) -> Bool {
        return compare(other) == .orderedDescending
    }
    
    public func isBefore(_ other: Date) -> Bool {
        return compare(other) == .orderedAscending
    }
    
    // MARK: - Statically Exposed Properties
    
    public static var now: Date {
        return Date()
    }
    
}
