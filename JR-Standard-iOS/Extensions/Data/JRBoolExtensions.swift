//
//  JRBoolExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

public extension Bool {
    
    public var toggled: Bool {
        return !self
    }
    
    public mutating func toggle() {
        self = toggled
    }
    
}
