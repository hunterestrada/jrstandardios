//
//  JRIntExtensions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

public extension Int {
    
    // MARK: - Exposed Properties
    
    var incremented: Int {
        return self + 1
    }
    
    var decremented: Int {
        return self - 1
    }
    
    // MARK: - Exposed Functions
    
    mutating func increment() {
        self += 1
    }
    
    mutating func decrement() {
        self -= 1
    }
    
}
