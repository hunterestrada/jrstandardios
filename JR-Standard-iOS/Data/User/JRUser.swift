//
//  JRUser.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/27/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

struct JRUser: JRDataModeling {
    
    // MARK: - Exposed Properties
    
    public var email: String
    public var auth: Auth?
    
    init(email e: String) {
        email = e
    }
    
    init(email e: String, auth a: Auth) {
        self.init(email: e)
        self.auth = a
    }
    
    struct Auth {
        
        var password: String?
        var token: String?
        var expiration: Date?
        
        init(password p: String, token t: String, expiration e: Date) {
            password = p
            token = t
            expiration = e
        }
        
    }

}
