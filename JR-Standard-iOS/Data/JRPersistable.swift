//
//  JRPersistable.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RealmSwift

public protocol JRPersistable: JRDataModeling, Hashable {
    
    // MARK: - Associated Types
    
    associatedtype Key: Hashable
    associatedtype Value
    
    // MARK: - Exposed Properties
    
    var key: Key { get }
    
    // MARK: - Statically Exposed Functions
    
    static func loaded(withKey key: Key) -> Value?
    
    func save()
    
}

// MARK: - Default Implementation

extension JRPersistable {
    
    public func save() {
        // TODO: Implement saving.
    }
    
    public var hashValue: Int {
        return key.hashValue
    }
    
    public static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.key == rhs.key
    }
    
    public static func loaded(withKey key: String) -> Self? {
        // TODO: Attempt loading user.
        return nil
    }
    
}
