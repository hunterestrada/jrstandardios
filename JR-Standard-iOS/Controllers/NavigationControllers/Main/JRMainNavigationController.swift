//
//  JRMainNavigationController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRMainNavigationController: JRNavigationController {
        
    // MARK: - Exposed Functions
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        connectUserSignInUpViewController(animated: false)
        if let _ = JRAuthenticationManager.authenticatedUser() {
            presentMainTabBarController(animated: false)
        }
        JRNotificationManager.add(
            self, with: #selector(userDidSignIn),
            for: JRAuthenticationManager.Notification.didSignInAuthenticatedUser
        )
        JRNotificationManager.add(
            self, with: #selector(userDidSignOut),
            for: JRAuthenticationManager.Notification.didSignOutAuthenticatedUser
        )
    }
    
    deinit {
        JRNotificationManager.remove(self)
    }
    
    // MARK: - Private Functions
    
    // MARK: Notifications
    
    @objc private func userDidSignIn() {
        presentMainTabBarController(animated: true)
    }
    
    @objc private func userDidSignOut() {
        dismissMainTabBarController()
    }
    
    // MARK: Transitions
    
    private func connectUserSignInUpViewController(animated: Bool) {
        let userSignInUpViewController = JRUserSignInUpViewController()
        userSignInUpViewController.viewModel = JRUserSignInUpViewModel()
        setViewControllers([userSignInUpViewController], animated: false)
    }
    
    private func presentMainTabBarController(animated: Bool) {
        let mainTabBarController = JRMainTabBarController()
        pushViewController(mainTabBarController, animated: animated)
    }
    
    private func dismissMainTabBarController() {
        popToRootViewController(animated: true)
    }
    
}
