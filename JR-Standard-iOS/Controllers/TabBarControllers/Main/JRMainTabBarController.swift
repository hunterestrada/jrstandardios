//
//  JRMainTabBarController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRMainTabBarController: JRTabBarController {
    
    // MARK: - Private Properties
    
    private var accountViewController: JRAccountViewController {
        let accountViewController = JRAccountViewController()
        accountViewController.viewModel = JRAccountViewModel()
        return accountViewController
    }
    
    private var feedViewController: JRFeedViewController {
        let feedViewController = JRFeedViewController()
        feedViewController.viewModel = JRFeedViewModel()
        return feedViewController
    }
    
    // MARK: - Exposed Properties
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers(
            [feedViewController, accountViewController],
            animated: false
        )
    }
    
}
