//
//  JRValidationManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

/**
 Contains functions to validate information within the application
 */
public struct JRValidationManager {
    
    // MARK: - Statically Private Properties
    
    private static var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = Constant.dateFormat
        return df
    }()
    
    // MARK: - Statically Exposed Functions
    
    /**
     Serves as an interface for functionality within 'JRMRegularExpression'
     - Parameter value: String to test as a match of 'regex'
     - Parameter regex: Regular expression to test 'value'
     - Returns: Boolean of whether the candidate is a match of the regular expression
     */
    public static func didValidate(_ value: String, as regex: JRRegularExpression) -> Bool {
        return regex.isSatisfied(by: value)
    }
    
    public static func date(from string: String) -> Date? {
        return dateFormatter.date(from: string)
    }
    
    // MARK: - Exposed Types
    
    /**
     Contains cases of regular expressions that can test matches with 'isSatisfied(by:)'
     To customize, change the cases and/or their related values.
     */
    public enum JRRegularExpression {
        
        // MARK: - Cases
        
        case email
        case password
        
        // MARK: - Private Properties
        
        /// Value of the regular expression
        private var value: String {
            switch self {
            case .email:
                return "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            case .password:
                return ".{7,30}"
            }
        }
        
        /// Predicate to test matches of the regular expression
        private var predicate: NSPredicate {
            return NSPredicate(format: "SELF MATCHES %@", value)
        }
        
        // MARK: - Private Functions
        
        /**
         Tests matches of the regular expression
         - Parameter candidate: String to test as a match of the regular expression
         - Returns: Boolean of whether the candidate is a match of the regular expression
         */
        fileprivate func isSatisfied(by candidate: String) -> Bool {
            return predicate.evaluate(with: candidate)
        }
        
    }
    
    // MARK: - Private Constants
    
    private struct Constant {
        static let dateFormat = "sdf"
    }
    
}
