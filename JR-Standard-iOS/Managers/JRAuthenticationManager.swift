//
//  JRAuthenticationManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import SwiftyJSON
import Foundation

public struct JRAuthenticationManager {
    
    // MARK: - Statically Private Properties
    
    /**
     Tracks state of checking authentication tokens after accessing authenticated user
     */
    private static var isCheckingAuthenticationTokens = false
    
    /**
     Ensures 'isCheckingAuthenticationTokens' is toggled when checking authentication tokens of authenticated user
     */
    private static let checkTokensCompletion = { (_ response: JRSuccessErrorResponse<NetworkError>) in
        isCheckingAuthenticationTokens.toggle()
    }
    
    /**
     Loads saved email from user defaults
     */
    private static var currentEmailAddress: String? {
        return JRDefaultsManager.load(Default.email)
    }
    
    /**
     Loads securely saved access token from device keychain
     */
    private static var currentAuthToken: String? {
        return JRKeychainManager.load(.accessToken)
    }
    
    private static var currentAuthExpiration: Date? {
        return Date.distantFuture
        // TODO: Load actual.
//        return JRDefaultsManager.load(Default.authExpiration)
    }
    
    /**
     Loads securely saved password from device keychain
     */
    private static var currentPassword: String? {
        return JRKeychainManager.load(.password)
    }
    
    // MARK: - Statically Exposed Functions
    
    // MARK: Authenticated User
    
    static func authenticatedUser(checkingAuthenticationTokens: Bool = true) -> JRUser? {
        guard let email = currentEmailAddress, email.isEmpty.toggled else {
            return nil
        }
        guard let password = currentPassword, password.isEmpty.toggled else {
            return nil
        }
        guard let token = currentAuthToken, let expiration = currentAuthExpiration, token.isEmpty.toggled else {
            return nil
        }
        let auth = JRUser.Auth(password: password, token: token, expiration: expiration)
        let user = JRUser(email: email, auth: auth)
        if checkingAuthenticationTokens && isCheckingAuthenticationTokens.toggled {
            // checkAuthenticationTokens(for: user)
        }
        return user
    }
    
    // MARK: Sign Out
    
    /*
     Sign Out removes credentials of the authenticated account from user defaults and the keychain.
     */
    
    /**
     Removes credentials of the authenticated user from user defaults and the keychain.
     */
    public static func signOutAuthenticatedUser() {
        JRDefaultsManager.save(nil, as: Default.email)
        JRDefaultsManager.save(nil, as: Default.authExpiration)
        JRKeychainManager.save(String.empty, as: .password)
        JRKeychainManager.save(String.empty, as: .accessToken)
    }
    
    // MARK: Sign Up
    
    /*
     Sign Up is divided into two parts: (1) account creation and (2) sign-in flow.
     */
    
    /**
     Creates a new account and proceeds to the sign-in flow.
     
     - Parameter email: The email of the new account
     - Parameter password: The password of the new account
     - Parameter completion: The handler of the calling site
     */
    static func attemptSigningUp(email e: String, password p: String, _ completion: @escaping SignInUpCompletion) {
        let parameters: [Parameter: String] = [.email: e, .password: p]
        JRNetworkManager.requestJSON(.post, to: Endpoint.users, passing: parameters, withAuthToken: nil) { response in
            switch response.result {
            case let .success(payload):
                // TODO: Collect expiration
                guard let e = payload[Parameter.email.key].string, let p = payload[Parameter.password.key].string, let t = payload[Parameter.authToken.key].string else {
                    return completion(.failure(.missingAuthenticatedUser))
                }
                saveUserCredentialsAtSignInUp(email: e, password: p, accessToken: t, accessTokenExpirationDate: Date.distantFuture, completion)
            case .failure:
                guard let _ = response.response?.statusCode else {
                    return completion(.failure(.networkConnection))
                }
                return completion(.failure(.missingAuthenticatedUser))
            }
        }
    }
    
    // MARK: Sign In
    
    /*
     Sign In is divided into three parts: (1) acquiring a user's access token, (2) saving their credentials, and (3) passing their account to the calling site.
     */
    
    /**
     Signs in the user and passes their authenticated account to the calling site.
     
     - Parameter email: The email of the authenticating user
     - Parameter password: The password of the authenticating user
     - Parameter completion: The handler of the calling site
     */
    static func attemptSigningIn(email e: String, password p: String, _ completion: @escaping SignInUpCompletion) {
        // Sign In (1)
//        refreshAccessToken(email: e, password: p) { response in
//            switch response {
//            case let .success(token, expiration):
////                saveUserCredentialsAtLogin(email: e, password: p, accessToken: accessToken, accessTokenExpirationDate: expirationDate, completion)
//            case let .failure(error):
//                return completion(.failure(error))
//            }
//        }
    }
    
    // MARK: - Statically Private Functions
    
    // MARK: Sign In
    
    // Sign In (2)
    private static func saveUserCredentialsAtSignInUp(email: String, password: String, accessToken: String, accessTokenExpirationDate: Date, _ completion: @escaping SignInUpCompletion) {
        JRDefaultsManager.save(email, as: Default.email)
        JRKeychainManager.save(password, as: .password)
        JRKeychainManager.save(accessToken, as: .accessToken)
        loadUserAtSignInUp(completion)
    }

    // Sign In (3)
    private static func loadUserAtSignInUp(_ completion: SignInUpCompletion) {
        guard let user = authenticatedUser(checkingAuthenticationTokens: false) else {
            return completion(.failure(.storingUser))
        }
        completion(.success(user))
        JRNotificationManager.post(Notification.didSignInAuthenticatedUser)
    }
    
    // MARK: Authentication Token Checking
    
    private static func checkAuthenticationTokens(for user: JRUser, _ completion: CheckTokenCompletion? = nil) {
        isCheckingAuthenticationTokens.toggle()
        if shouldRefreshAccessToken(for: user) {
            refreshAccessToken(for: user, checkTokensCompletions(with: completion))
        } else {
            isCheckingAuthenticationTokens.toggle()
            checkTokensCompletions(with: completion).forEach { completion in
                completion(.success)
            }
        }
    }

    private static func shouldRefreshAccessToken(for user: JRUser) -> Bool {
        guard let _ = user.auth?.token else {
            return true
        }
        guard let expiration = user.auth?.expiration else {
            return true
        }
        return shouldRefreshAccessToken(expiringAt: expiration)
    }
    
    private static func shouldRefreshAccessToken(expiringAt expiration: Date) -> Bool {
        guard expiration.isBefore(Date.now) else {
            return true
        }
        return abs(expiration.timeIntervalSinceNow) < Constant.minimumAccessTokenTimeIntervalUntilRefresh
    }

    private static func checkTokensCompletions(with customCompletion: CheckTokenCompletion? = nil) -> [CheckTokenCompletion] {
        if let completion = customCompletion {
            return [completion, checkTokensCompletion]
        }
        return [checkTokensCompletion]
    }
    
    // MARK: Authentication Token Refreshing
    
    private static func refreshAccessToken(for user: JRUser, _ completions: [CheckTokenCompletion], _ other: CheckTokenCompletion? = nil) {
        guard user.email.isEmpty.toggled else {
            return completions.forEach { completion in
                completion(.failure(.missingEmail))
            }
        }
        guard let password = user.auth?.password, password.isEmpty.toggled else {
            return completions.forEach { completion in
                completion(.failure(.missingPassword))
            }
        }
        refreshAccessToken(email: user.email, password: password) { response in
            switch response {
            case let .success(token, expiration):
                handleRefreshAccessTokenSuccessResponse(for: user, token: token, expiration: expiration, completions)
            case let .failure(error):
                handleRefreshAccessTokenFailureResponse(with: error, completions)
            }
        }
    }
    
    private static func refreshAccessToken(email: String, password: String, _ completion: @escaping TokenRefreshCompletion) {
//        JRNetworkManager.requestJSON(.get, to: Endpoint.users, passing: [Parameter.email: email, Parameter.password: password], withAuthToken: nil) { response in
//            switch response.result {
//            case let .success(payload):
//                handleRefreshAccessTokenSuccessfulResponse(with: payload, completion)
//            case .failure:
//                guard let value = response.response?.statusCode else {
//                    return completion(.failure(.networkConnection))
//                }
//                handleRefreshAccessTokenFailureResponse(with: JRStatusCode(rawValue: value), completion)
//            }
//        }
    }
    
    private static func handleRefreshAccessTokenSuccessResponse(for user: JRUser, token: String, expiration: Date, _ completions: [CheckTokenCompletion]) {
        JRKeychainManager.save(token, as: .accessToken)
        completions.forEach { completion in
            completion(.success)
        }
    }
    
    private static func handleRefreshAccessTokenFailureResponse(with error: NetworkError, _ completions: [CheckTokenCompletion]) {
        completions.forEach { completion in
            completion(.failure(error))
        }
    }
    
    private static func handleRefreshAccessTokenSuccessfulResponse(with payload: JSON, _ completion: @escaping TokenRefreshCompletion) {
        guard let token = payload[Parameter.authToken.key].string, token.isEmpty.toggled else {
            return completion(.failure(.missingAccessToken))
        }
        guard let rawExpiration = payload[Parameter.authExpiration.key].string else {
            return completion(.failure(.missingAccessTokenExpirationDate))
        }
        guard let expiration = JRValidationManager.date(from: rawExpiration) else {
            return completion(.failure(.missingAccessTokenExpirationDate))
        }
        return completion(.success(token, expiration))
    }
    
    private static func handleRefreshAccessTokenFailureResponse(with statusCode: JRStatusCode?, _ completion: TokenRefreshCompletion) {
        if let sc = statusCode {
            switch sc {
            case .forbidden, .unauthorized:
                signOutAuthenticatedUser()
            default:
                break
            }
        }
        return completion(.failure(.invalidAccessTokenResponse))
    }
    
    // MARK: - Exposed Types
    
    typealias CheckTokenCompletion = (JRSuccessErrorResponse<NetworkError>) -> Void
    typealias TokenRefreshCompletion = (JRValueErrorResponse<(String, Date), NetworkError>) -> Void
    typealias SignInUpCompletion = (JRValueErrorResponse<JRUser, NetworkError>) -> Void
    
    // MARK: - Private Types
    
    // MARK: Constants
    
    private struct Constant {
        static let minimumAccessTokenTimeIntervalUntilRefresh = Constant.accessTokenLifetimeInSeconds.divided(by: JRNumber.two.double)
        static let accessTokenLifetimeInSeconds = 86400.0 // 24 hours
    }
    
    // MARK: Errors
    
    public enum NetworkError: Error {
        case missingAuthenticatedUser
        case missingEmail
        case missingPassword
        case missingAccessToken
        case missingAccessTokenExpirationDate
        case missingSessionToken
        case missingSessionExpirationDate
        case storingUser
        case invalidAccessTokenResponse
        case invalidSessionTokenResponse
        case networkConnection
    }
    
    // MARK: - Notifications
    
    public enum Notification: String, JRNotifying {
        
        case didSignInAuthenticatedUser = "did_sign_in_authenticated_user"
        case didSignOutAuthenticatedUser = "did_sign_out_authenticated_user"
        
        public var name: String {
            return rawValue
        }
        
    }
    
    public enum Endpoint: String, JREndpointing {
        
        case users = "users/"
        
        public var baseLink: String? {
            return "http://127.0.0.1:5000/"
        }
        
        public var link: String {
            return rawValue
        }
        
    }
    
    public enum Parameter: String, JRParametering {
        
        case email = "email"
        case password = "password"
        case authToken = "auth_token"
        case authExpiration = "auth_expiration"
        
        public var key: String {
            return rawValue
        }
        
    }
    
    // MARK: Credentials
    
    private enum Default: String, JRDefaulting {
        
        case email = "key_authenticated_user_email"
        case authExpiration = "key_authenticated_user_expiration"
        
        var key: String {
            return rawValue
        }
        
    }
    
}
