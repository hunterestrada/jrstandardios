//
//  JRAppearanceManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

struct JRAppearanceManager {
    
    struct Constant {
        struct Window {
            static let backgroundColor = UIColor.white
        }
    }
    
}
