//
//  JRNotificationManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

/**
 Contains functions to abstract 'NotificationCenter' and
 reduce boiler-plate code dealing with 'Notification'.
 */
public struct JRNotificationManager {
    
    // MARK: - Statically Private Properties
    
    private static let standard = NotificationCenter.default
    
    // MARK: - Statically Exposed Functions
    
    public static func post(_ notifying: JRNotifying) {
        standard.post(notifying.notification)
    }
    
    public static func add(_ observer: Any, with selector: Selector, for name: Notification.Name) {
        standard.addObserver(observer, selector: selector, name: name, object: nil)
    }
    
    public static func add(_ observer: Any, with selector: Selector, for notifying: JRNotifying) {
        standard.addObserver(observer, selector: selector, name: notifying.notification.name, object: nil)
    }
    
    public static func remove(_ observer: Any, for name: Notification.Name) {
        standard.removeObserver(observer, name: name, object: nil)
    }
    
    public static func remove(_ observer: Any, for notifying: JRNotifying) {
        standard.removeObserver(observer, name: notifying.notification.name, object: nil)
    }
    
    public static func remove(_ observer: Any) {
        standard.removeObserver(observer)
    }
    
}
