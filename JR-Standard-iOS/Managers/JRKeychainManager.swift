//
//  JRKeychainManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

/**
 Manages interaction with the device's keychain.
 Should store passwords, access tokens, and session tokens.
 */
public struct JRKeychainManager {
    
    // MARK: - Statically Exposed Functions
    
    /**
     Saves value into the keychain as the provided credential
     
     - Parameter value: A String to be saved as a credential
     - Parameter credential: A Credential to classify the value for retrieval
     - Parameter completion: A closure that passes the SaveResult to the caller
     */
    public static func save(_ value: String, as credential: Credential, _ completion: ((_ succeeded: SaveResult) -> Void)? = nil) {
        if let data = data(from: value) {
            let query = saveQuery(for: credential, of: data) as CFDictionary
            SecItemDelete(query)
            if SecItemAdd(query, nil) == errSecSuccess {
                completion?(.success)
            } else {
                completion?(.failure)
            }
        } else {
            completion?(.failure)
        }
    }
    
    /**
     Loads value from the keychain as the provided credential
     
     - Parameter credential: A Credential to be loaded
     - Returns: The saved value for 'credential'
     */
    public static func load(_ credential: Credential) -> String? {
        let query = loadQuery(for: credential) as CFDictionary
        var result: AnyObject?
        let status = SecItemCopyMatching(query, &result)
        guard let data = result as? Data, status == errSecSuccess else {
            return nil
        }
        return string(from: data)
    }
    
    // MARK: - Statically Private Functions
    
    private static func data(from string: String) -> Data? {
        return string.data(using: Constant.encoding, allowLossyConversion: false)
    }
    
    private static func string(from data: Data) -> String? {
        return String(data: data, encoding: Constant.encoding)
    }
    
    private static func saveQuery(for credential: Credential, of data: Data) -> [String : Any] {
        return [Secure.classKey.string: Secure.classValue.string, Secure.attributeServiceKey.string: credential.key, Secure.attributeAccountKey.string: Secure.attributeAccountValue.string, Secure.dataKey.string: data]
    }
    
    private static func loadQuery(for credential: Credential) -> [String : Any] {
        return [Secure.classKey.string: Secure.classValue.string, Secure.attributeServiceKey.string: credential.key, Secure.attributeAccountKey.string: Secure.attributeAccountValue.string, Secure.returnDataKey.string: true, Secure.matchLimitKey.string: Secure.matchLimitValue.string]
    }
    
    // MARK: - Public Types
    
    /**
     To customize, change the cases and/or their computed properties.
     */
    public enum Credential {
        
        case password
        case accessToken
        case sessionToken
        
        var key: String {
            switch self {
            case .password:
                return "password"
            case .accessToken:
                return "access_token"
            case .sessionToken:
                return "session_token"
            }
        }
        
        var keyData: Data {
            return key.data(using: .utf8, allowLossyConversion: false) ?? Data()
        }
        
    }
    
    public enum SaveResult: Int {
        case success, failure
    }
    
    // MARK: - Private Types
    
    /**
     Abstracts key-value pairs to save/load values into/from the keychain.
     */
    private enum Secure {
        
        case classKey, classValue
        case attributeAccountKey, attributeAccountValue
        case attributeServiceKey, attributeServiceValue
        case dataKey
        case returnDataKey
        case matchLimitKey
        case matchLimitValue
        
        private var encoded: CFString {
            switch self {
            case .classKey:
                return kSecClass
            case .classValue:
                return kSecClassGenericPassword
            case .attributeAccountKey:
                return kSecAttrAccount
            case .attributeAccountValue:
                return Constant.authenticatingAccount
            case .attributeServiceKey:
                return kSecAttrService
            case .attributeServiceValue:
                return Constant.authenticatingService
            case .dataKey:
                return kSecValueData
            case .returnDataKey:
                return kSecReturnData
            case .matchLimitKey:
                return kSecMatchLimit
            case .matchLimitValue:
                return kSecMatchLimitOne
            }
        }
        
        var string: String {
            return String(format: encoded as String)
        }
        
    }
    
    // MARK: - Constants
    
    private struct Constant {
        static let authenticatingAccount = "authenticating_account" as CFString
        static let authenticatingService = "authenticating_service" as CFString
        static let encoding: String.Encoding = .utf8
    }
    
}
