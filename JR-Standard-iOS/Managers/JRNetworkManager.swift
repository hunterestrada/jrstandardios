//
//  JRNetworkManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Foundation

public struct JRNetworkManager {
    
    // MARK: - Statically Exposed Functions
    
    /**
     Makes a request and converts the successful result to JSON
     before calling the completion handler.
     It's a bit messy but improves readability/usability at other sites.
     */
    static func requestJSON<Parameter: JRParametering>(
        _ method: HTTPMethod,
        to endpoint: JREndpointing,
        passing params: [Parameter : Any] = [Parameter : Any](),
        withAuthToken token: String? = nil,
        _ completion: @escaping JSONRequestCompletion
    ) {
        let headers = headersWithCurrentTokens(for: endpoint, withAuthToken: token)
        let parameters = passableParameters(from: params)
        intercept(method, to: endpoint, passing: params, withAccessToken: token)
        validateJSONResponse(of: method, to: endpoint, withHeaders: headers, passing: parameters, withAccessToken: token, completion)
    }
    
    /**
     Creates a request for another networking site.
     */
    static func create<Parameter>(
        _ method: HTTPMethod,
        to endpoint: JREndpointing,
        passing p: [Parameter : Any] = [Parameter : Any]()
    ) -> URLRequest where Parameter: JRParametering {
        let headers = headersWithCurrentTokens(for: endpoint)
        let parameters = passableParameters(from: p)
        return Alamofire.request(
            endpoint.url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers
        ).request?.urlRequest ?? NSURLRequest() as URLRequest
    }
    
    // MARK: - Statically Private Functions
    
    private static func validateJSONResponse(
        of method: HTTPMethod,
        to endpoint: JREndpointing,
        withHeaders headers: [String: String],
        passing parameters: [String: Any],
        withAccessToken t: String? = nil,
        _ completion: @escaping JSONRequestCompletion
    ) {
        Alamofire.request(
            endpoint.url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers
        ).validate().responseJSON { r in
            let response = jsonResponse(from: r)
            intercept(response, of: method, to: endpoint, afterPassing: parameters)
            completion(response)
        }
    }
    
    private static func jsonResponse(from r: DataResponse<Any>) -> DataResponse<JSON> {
        return DataResponse(
            request: r.request,
            response: r.response,
            data: r.data,
            result: jsonResult(of: r)
        )
    }
    
    private static func jsonResult(of response: DataResponse<Any>) -> Result<JSON> {
        switch response.result {
        case let .success(value):
            return .success(JSON(value))
        case let .failure(error) where response.response?.statusCode != JRStatusCode.ok.rawValue:
            logErroneous(response, with: error)
            return .failure(error)
        default:
            return .success(JSON(JRStatusCode.ok.rawValue))
        }
    }
    
    private static func headersWithCurrentTokens(for endpoint: JREndpointing, withAuthToken authToken: String? = nil) -> [String : String] {
        let headers = Parameter.pairing(.apiClientId) as? [String: String] ?? [String: String]()
        if let t = authToken ?? JRAuthenticationManager.authenticatedUser()?.auth?.token {
            return headers + [Parameter.authorization.key: Constant.accessTokenHeaderPrefix + t]
        }
        return headers
    }
    
    /**
     Intercepts any requests passed through JRNetworkManager.
     */
    private static func intercept<Parameter>(_ method: HTTPMethod, to endpoint: JREndpointing, passing parameters: [Parameter : Any], withAccessToken accessToken: String? = nil) where Parameter: JRParametering {

    }
    
    /**
     Intercepts any responses returned to JRNetworkManager.
     */
    private static func intercept(_ dataResponse: DataResponse<JSON>, of m: HTTPMethod, to e: JREndpointing, afterPassing p: [String : Any]) {
        switch dataResponse.result {
        case .failure:
            guard let code = dataResponse.response?.statusCode else {
                return
            }
            switch code {
            case JRStatusCode.forbidden.rawValue, JRStatusCode.unauthorized.rawValue:
                JRAuthenticationManager.signOutAuthenticatedUser()
            default: break
            }
        default: break
        }
    }
    
    /*
     Converts parameters from enum types to raw values.
     */
    private static func passableParameters<Parameter>(from p: [Parameter : Any]) -> [String : Any] where Parameter: JRParametering {
        var parameters = [String : Any]()
        p.forEach { parameter, val in
            parameters[parameter.key] = val
        }
        return parameters
    }
    
    /**
     Logs errors of failed responses.
     */
    private static func logErroneous(_ response: DataResponse<Any>, with e: Error) {
        print("\(String(describing: JRNetworkManager.self)) Error: \(response.response?.statusCode ?? JRNumber.zero.int) - \(e.localizedDescription)")
    }
    
    // MARK: - Exposed types
    
    typealias JSONRequestCompletion = (DataResponse<JSON>) -> Void
    typealias HTMLRequestCompletion = (DataResponse<NSString>) -> Void
    typealias ValidateHeadersCompletion = (JRSuccessErrorResponse<JRAuthenticationManager.NetworkError>) -> Void
    
    // MARK: - Private Types
    
    private enum Parameter: String, JRParametering {
        
        case apiClientId = "api_client_id"
        case authorization = "Authorization"
        
        var key: String {
            return rawValue
        }
        
        var value: String {
            switch self {
            case .apiClientId:
                return key // TODO: Replace with actual id.
            default:
                return String.empty
            }
        }
        
    }
    
    // MARK: - Private Constants
    
    private struct Constant {
        static let accessTokenHeaderPrefix = "Bearer "
    }
    
}
