//
//  JRDefaultsManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

public struct JRDefaultsManager {
    
    // MARK: - Statically Private Properties
    
    private static let standard = UserDefaults.standard
    
    // MARK: - Statically Exposed Functions
    
    public static func load<T>(_ defaulting: JRDefaulting) -> T? {
        return standard.value(forKey: defaulting.key) as? T
    }
    
    public static func save(_ value: Any?, as defaulting: JRDefaulting) {
        standard.set(value, forKey: defaulting.key)
        standard.synchronize()
    }
    
    // MARK: - Exposed Types
    
    public enum SharedDefault: JRDefaulting {
        
        case example
        
        public var key: String {
            switch self {
            case .example:
                return "key_example"
            }
        }
        
    }
    
}
