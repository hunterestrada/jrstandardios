//
//  JRApplicationManager.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

struct JRApplicationManager {
    
    static func start(_ application: UIApplication, in window: UIWindow, withLaunchOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window.backgroundColor = JRAppearanceManager.Constant.Window.backgroundColor
        window.rootViewController = JRMainNavigationController()
        window.makeKeyAndVisible()
        return true
    }
    
}
