//
//  JRAnimationDuration.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/29/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

enum JRAnimationDuration {
    
    case none, verySlow, slow, fast, veryFast
    
    var duration: TimeInterval {
        switch self {
        case .none:
            return 0.00
        case .veryFast:
            return 0.25
        case .fast:
            return 0.50
        case .slow:
            return 1.00
        case .verySlow:
            return 2.00
        }
    }
    
    func animate(_ handler: @escaping () -> Void) {
        UIView.animate(withDuration: duration, animations: handler)
    }
    
    static func animate(with duration: TimeInterval, using options: UIViewAnimationOptions, _ handler: @escaping () -> Void) {
        UIView.animate(withDuration: duration, animations: handler)
    }
    
}
