//
//  JRStatusCode.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

enum JRStatusCode: Int {
    
    // MARK: - Unknown
    
    case unknown = 0
    
    // MARK: - Normal
    
    case ok = 200
    
    // MARK: - Client Errors
    
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    
}
