//
//  JRNumber.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

/**
 Contains properties of commonly used numbers so that
 their raw values can be abstracted from code
 */
public enum JRNumber {
    
    // MARK: - Cases
    
    case zero, half, one, two
    
    // MARK: - Exposed Properties
    
    public var int: Int {
        return Int(float)
    }
    
    public var float: Float {
        switch self {
        case .zero:
            return 0.0
        case .half:
            return 0.5
        case .one:
            return 1.0
        case .two:
            return 2.0
        }
    }
    
    public var cgFloat: CGFloat {
        return CGFloat(float)
    }
    
    public var double: Double {
        return Double(float)
    }
    
    public var timeInterval: TimeInterval {
        return TimeInterval(float)
    }
    
}
