//
//  JRSuccessErrorResponse.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

public enum JRSuccessErrorResponse<T> where T: Error {
    
    case success
    case failure(T)
    
}
