//
//  JRValueErrorResponse.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

public enum JRValueErrorResponse<V, E> where E: Error {
    
    case success(V)
    case failure(E)
    
}
