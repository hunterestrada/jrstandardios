//
//  DataNode.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

class DataNode<T> {
    
    let value: T
    
    var lhs: DataNode<T>?
    var rhs: DataNode<T>?
    
    init(_ value: T) {
        self.value = value
    }
    
}
