//
//  LinkedList.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

struct LinkedList<T> {
    
    var head: T? {
        return headNode?.value
    }
    
    var tail: T? {
        return tailNode?.value
    }
    
    // MARK: - Private Properties
    
    private var headNode: DataNode<T>? = nil
    private var tailNode: DataNode<T>? = nil
    
    init() {
    }
    
    init(_ values: [T]) {
        values.forEach { value in
            attachTail(with: value)
        }
    }
    
    // MARK:
    
    mutating func removeHead() -> T? {
        let value = headNode?.value
        if headNode === tailNode {
            headNode = nil
            tailNode = nil
        } else {
            headNode = headNode?.rhs
            headNode?.lhs = nil
        }
        return value
    }
    
    mutating func removeTail() -> T? {
        let value = tailNode?.value
        if headNode === tailNode {
            headNode = nil
            tailNode = nil
        } else {
            tailNode = tailNode?.lhs
            tailNode?.rhs = nil
        }
        return value
    }
    
    mutating func attachHead(with value: T) {
        let newHead = DataNode(value)
        if let oldHead = headNode {
            oldHead.lhs = newHead
            newHead.rhs = oldHead
        } else {
            tailNode = newHead
        }
        headNode = newHead
    }
    
    mutating func attachHead(with values: [T]) {
        values.reversed().forEach { value in
            attachHead(with: value)
        }
    }
    
    mutating func attachTail(with value: T) {
        let newTail = DataNode(value)
        if let oldTail = tailNode {
            oldTail.rhs = newTail
            newTail.lhs = oldTail
        } else {
            headNode = newTail
        }
        tailNode = newTail
    }
    
    mutating func attachTail(with values: [T]) {
        values.forEach { value in
            attachTail(with: value)
        }
    }
    
}
