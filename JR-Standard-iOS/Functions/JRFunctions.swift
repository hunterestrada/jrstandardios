//
//  JRFunctions.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/28/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//


public func +<Key, Value>(lhs: [Key: Value], rhs: [Key: Value]) -> [Key: Value] {
    var pairs = lhs
    rhs.forEach { key, value in
        pairs[key] = value
    }
    return pairs
}
