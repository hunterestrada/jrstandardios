//
//  JRAccountViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

final class JRAccountViewController: JRInstantiatedViewBindingController<JRAccountViewModel, JRAccountView> {
    
    // MARK: - Exposed Properties
    
    override var preferredNavigationBarTitle: String? {
        return Constant.navigationBarTitle
    }
    
    override var preferredTabBarTitle: String? {
        return Constant.tabBarTitle
    }
    
    override var preferredTabBarImage: UIImage? {
        return Constant.tabBarImage
    }
    
    override var preferredNavigationBarButtonItemsRight: [UIBarButtonItem] {
        return [settingsBarButtonItem]
    }
    
    // MARK: - Private Properties
    
    private var settingsBarButtonItem: UIBarButtonItem {
        return UIBarButtonItem(image: #imageLiteral(resourceName: "settings_button"), style: .plain, target: self, action: #selector(userDidTapSettings))
    }
    
    private var settingsNavigationController: JRSettingsNavigationController {
        let nc = JRSettingsNavigationController()
        nc.dismissHandler = { [weak self] in
            guard let w = self else { return }
            w.dismiss(animated: true, completion: nil)
        }
        return nc
    }
    
    // MARK: - Exposed Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let c = JRAccountDetailView.Constant.subtitleLabel.self
        v.emailView.subtitleLabel.text = c.emailText
        v.passwordView.subtitleLabel.text = c.passwordText
        v.authTokenView.subtitleLabel.text = c.authTokenText
    }
    
    override func bind() {
        super.bind()
        guard let vm = viewModel else { return }
        // MARK: Take
        vm.emailTitleLabelText.bind(
            to: v.emailView.titleLabel.rx.text
        ).disposed(by: disposer)
        vm.passwordTitleLabelText.bind(
            to: v.passwordView.titleLabel.rx.text
        ).disposed(by: disposer)
        vm.tokenTitleLabelText.bind(
            to: v.authTokenView.titleLabel.rx.text
        ).disposed(by: disposer)
    }
    
    // MARK: - Private Functions
    
    // MARK: User Actions
    
    @objc private func userDidTapSettings() {
        present(settingsNavigationController, animated: true, completion: nil)
    }
    
    // MARK: - Constants
    
    struct Constant {
        static let navigationBarTitle = "My Account".localizedCapitalized
        static let tabBarTitle = "Account".localizedCapitalized
        static let tabBarImage = #imageLiteral(resourceName: "account_tab")
    }
    
}
