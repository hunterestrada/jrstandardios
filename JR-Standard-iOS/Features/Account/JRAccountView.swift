//
//  JRAccountView.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRAccountView: JRView {
    
    @IBOutlet var emailView: JRAccountDetailView!
    
    @IBOutlet var passwordView: JRAccountDetailView!
    
    @IBOutlet var authTokenView: JRAccountDetailView!
    
}
