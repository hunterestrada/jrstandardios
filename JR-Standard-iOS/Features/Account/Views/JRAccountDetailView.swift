//
//  JRAccountDetailView.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRAccountDetailView: JROwningView {
    
    // MARK: - Exposed Properties
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var subtitleLabel: UILabel!
    
    // MARK: - Constants
    
    struct Constant {
        struct subtitleLabel {
            static let emailText = "Email".localizedCapitalized
            static let passwordText = "Password".localizedCapitalized
            static let authTokenText = "Token".localizedCapitalized
        }
    }
    
}
