//
//  JRAccountViewModel.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/3/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RxSwift

final class JRAccountViewModel: JRViewModel {
    
    // MARK: - Exposed Properties
    
    let emailTitleLabelText: Observable<String?>
    let passwordTitleLabelText: Observable<String?>
    let tokenTitleLabelText: Observable<String?>
    
    // MARK: - Private Properties
    
    // MARK: State
    
    private let email = Variable<String?>(nil)
    private let password = Variable<String?>(nil)
    private let token = Variable<String?>(nil)
    
    // MARK: - Exposed Functions
    
    // MARK: Lifecycle
    
    override init() {
        emailTitleLabelText = email.asObservable()
        passwordTitleLabelText = password.asObservable()
        tokenTitleLabelText = token.asObservable()
        super.init()
        let notifications: [JRAuthenticationManager.Notification] = [.didSignInAuthenticatedUser, .didSignOutAuthenticatedUser]
        notifications.forEach { n in
            JRNotificationManager.add(self, with: #selector(didSetUser), for: n)
        }
        didSetUser()
    }
    
    // MARK: - Private Functions
    
    // MARK: Notifications
    
    @objc private func didSetUser() {
        updateAccountProperties()
    }
    
    // MARK: Updates
    
    private func updateAccountProperties() {
        let user = JRAuthenticationManager.authenticatedUser()
        print(user?.email ?? String.empty)
        email.value = user?.email
        password.value = user?.auth?.password
        token.value = user?.auth?.token
    }
    
}
