//
//  JRSettingsViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/23/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRSettingsViewController: JRInstantiatedViewBindingController<JRSettingsViewModel, JRSettingsView> {
    
    // MARK: - Exposed Properties
    
    override var preferredNavigationBarTitle: String? {
        return Constant.NavigationBar.title
    }
    
    override var preferredNavigationBarButtonItemsRight: [UIBarButtonItem] {
        return [cancelBarButtonItem]
    }
    
    // MARK: - Private Properties
    
    private var cancelBarButtonItem: UIBarButtonItem {
        return UIBarButtonItem(title: String.cancel, style: .plain, target: self, action: #selector(userDidTapCancel))
    }
    
    // MARK: - Private Functions
    
    // MARK: User Actions

    @objc func userDidTapCancel() {
        dismissHandler?()
    }
    
    // MARK: - Private Constants
    
    private struct Constant {
        struct NavigationBar {
            static let title = "My Settings".localizedCapitalized
        }
    }
    
}
