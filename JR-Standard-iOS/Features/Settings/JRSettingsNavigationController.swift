//
//  JRSettingsNavigationController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/23/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRSettingsNavigationController: JRNavigationController {
    
    // MARK: - Private Properties
    
    private var settingsViewController: JRSettingsViewController {
        let settingsViewController = JRSettingsViewController()
        settingsViewController.viewModel = JRSettingsViewModel()
        settingsViewController.dismissHandler = dismissHandler
        return settingsViewController
    }
    
    // MARK: - Exposed Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers([settingsViewController], animated: false)
    }
    
}
