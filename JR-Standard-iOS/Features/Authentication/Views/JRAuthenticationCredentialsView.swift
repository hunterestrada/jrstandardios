//
//  JRAuthenticationCredentialsView.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/27/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRAuthenticationCredentialsView: JROwningView {
    
    // MARK: - Exposed Properties
    
    @IBOutlet var emailTextField: UITextField! {
        didSet {
            emailTextField.placeholder = Constant.emailTextField.placeholder
            emailTextField.textColor = Constant.anyTextField.textColor
        }
    }
    
    @IBOutlet var passwordTextField: UITextField! {
        didSet {
            passwordTextField.placeholder = Constant.passwordTextField.placeholder
            passwordTextField.textColor = Constant.anyTextField.textColor
        }
    }
    
    @IBOutlet var submitButton: UIButton! {
        didSet {
            submitButton.setTitle(Constant.submitButton.signInTitle, for: .normal)
        }
    }
    
    // MARK: - Private Types
    
    // MARK: - Constants
    
    struct Constant {
        struct anyTextField {
            static let textColor = UIColor.darkText
            static let validTextColor = UIColor.from(r: 60, g: 184, b: 84)
            static let invalidTextColor = UIColor.from(r: 219, g: 50, b: 54)
        }
        struct emailTextField {
            static let placeholder = "Email".localizedCapitalized
        }
        struct passwordTextField {
            static let placeholder = "Password".localizedCapitalized
        }
        struct submitButton {
            static let signInTitle = "Sign In".localizedCapitalized
            static let signUpTitle = "Sign Up".localizedCapitalized
        }
    }
    
}
