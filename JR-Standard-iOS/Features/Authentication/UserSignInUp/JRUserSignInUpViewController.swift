//
//  JRUserSignInUpViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/27/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RxSwift
import RxCocoa

final class JRUserSignInUpViewController: JRInstantiatedViewBindingController<JRUserSignInUpViewModel, JRUserSignInUpView>, JRKeyboardPresenting {
    
    // MARK: - Exposed Properties
    
    override var prefersNavigationBarHidden: Bool {
        return true
    }
    
    override var preferredBackBarButtonItemTitle: String? {
        return Constant.backBarButtonItemTitle
    }
    
    // MARK: - Exposed Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: Add tap gesture recognizer to v
    }
    
    override func bind() {
        super.bind()
        guard let vm = viewModel else { return }
        bindSelf(to: vm)
        bindCredentialsView(to: vm)
        bindSubmitButton(to: vm)
        bindSignInUpButton(to: vm)
    }
    
    // MARK: JRKeyboardPresenting
    
    func keyboardWillShow(_ keyboard: JRKeyboard) {
        v.animateBottomPaddingConstraint(
            to: keyboard.height,
            withDuration: keyboard.animationDuration,
            using: keyboard.animationOptions
        )
    }
    
    func keyboardWillHide(_ keyboard: JRKeyboard) {
        v.animateBottomPaddingConstraint(
            to: JRNumber.zero.cgFloat,
            withDuration: keyboard.animationDuration,
            using: keyboard.animationOptions
        )
    }
    
    // MARK: - Private Functions
    
    // MARK: Actions
    
    @objc private func userDidTapView() {
        viewModel?.userDidTapView()
    }
        
    // MARK: Binders
    
    private func bindSelf(to vm: ViewModel) {
        // MARK: Take
        vm.isPresentingLoadingView.bind { [weak self] isPresenting in
            guard let weak = self else { return }
            if isPresenting {
                weak.presentLoadingViewController()
            } else {
                weak.dismissLoadingViewController()
            }
        }.disposed(by: disposer)
    }
    
    private func bindCredentialsView(to vm: ViewModel) {
        // MARK: Give
        v.credentialsView.emailTextField.rx.text.asObservable().bind { text in
            vm.userDidSetEmail(to: text ?? String.empty)
        }.disposed(by: disposer)
        v.credentialsView.passwordTextField.rx.text.asObservable().bind { text in
            vm.userDidSetPassword(to: text ?? String.empty)
        }.disposed(by: disposer)
        // MARK: Take
        vm.emailTextColor.bind { [weak self] color in
            guard let weak = self else { return }
            weak.v.credentialsView.emailTextField.textColor = color
        }.disposed(by: disposer)
        vm.passwordTextColor.bind { [weak self] color in
            guard let weak = self else { return }
            weak.v.credentialsView.passwordTextField.textColor = color
        }.disposed(by: disposer)
    }
    
    private func bindSubmitButton(to vm: ViewModel) {
        // MARK: Give
        v.credentialsView.submitButton.rx.tap.asObservable().bind {
            vm.userDidTapSubmitButton()
        }.disposed(by: disposer)
        // MARK: Take
        vm.submitButtonTitle.bind(
            to: v.credentialsView.submitButton.rx.title(for: .normal)
        ).disposed(by: disposer)
        vm.isSubmitButtonEnabled.bind(
            to: v.credentialsView.submitButton.rx.isEnabled
        ).disposed(by: disposer)
    }
    
    private func bindSignInUpButton(to vm: ViewModel) {
        // MARK: Give
        v.signInUpButton.rx.tap.asObservable().bind {
            vm.userDidTapSignInUpButton()
        }.disposed(by: disposer)
        // MARK: Take
        vm.signInUpButtonTitle.bind(
            to: v.signInUpButton.rx.title(for: .normal)
        ).disposed(by: disposer)
    }
    
    // MARK: Transitions
    
    private func presentLoadingViewController() {
        let vc = JRLoadingViewController()
        vc.viewModel = JRLoadingViewModel()
        vc.dismissHandler = {
            if vc.isBeingDismissed.toggled {
                vc.dismiss(animated: false, completion: nil)
            }
        }
        present(vc, animated: false, completion: nil)
    }
    
    private func dismissLoadingViewController() {
        if let vc = presentedViewController as? JRLoadingViewController {
            vc.dismissHandler?()
        }
    }
    
    // MARK: - Private Constants
    
    private struct Constant {
        static let backBarButtonItemTitle = "Sign Out".localizedCapitalized
    }
    
}
