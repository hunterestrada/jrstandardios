//
//  JRUserSignInUpView.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/27/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRUserSignInUpView: JRView {
    
    // MARK: - Exposed Properties
    
    @IBOutlet var titleLabel: UILabel! {
        didSet {
            titleLabel.text = Constant.TitleLabel.text
        }
    }
    
    @IBOutlet var credentialsView: JRAuthenticationCredentialsView!
    
    @IBOutlet var signInUpButton: UIButton! {
        didSet {
            signInUpButton.setTitle(Constant.SignInUpButton.signInTitle, for: .normal)
        }
    }
    
    @IBOutlet private var bottomPaddingConstraint: NSLayoutConstraint!
    
    // MARK: - Exposed Functions
    
    func animateBottomPaddingConstraint(to constant: CGFloat, withDuration duration: TimeInterval, using options: UIViewAnimationOptions) {
        JRAnimationDuration.animate(with: duration, using: options) { [weak self] in
            guard let weak = self else { return }
            weak.bottomPaddingConstraint.constant = constant
            weak.layoutIfNeeded()
        }
    }
    
    // MARK: - Constants
    
    struct Constant {
        struct TitleLabel {
            static let text = "Authentication".localizedCapitalized
        }
        struct SignInUpButton {
            static let signUpTitle = "Don't have an account?".localizedCapitalized
            static let signInTitle = "Already have an account?".localizedCapitalized
        }
    }
    
}
