//
//  JRUserSignInUpViewModel.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/27/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RxSwift

final class JRUserSignInUpViewModel: JRViewModel {
    
    // MARK: - Exposed Properties
    
    let isPresentingLoadingView: Observable<Bool>
    
    let isSubmitButtonEnabled: Observable<Bool>
    
    let emailTextColor: Observable<UIColor>
    let passwordTextColor: Observable<UIColor>
    
    let submitButtonTitle: Observable<String>
    let signInUpButtonTitle: Observable<String>
    
    // MARK: - Private Properties
    
    private let email = Variable(String.empty)
    private let password = Variable(String.empty)
    private let submitButtonAction = Variable(SubmitButtonAction.signIn)
    private let isLoading = Variable(false)
    
    override init() {
        isPresentingLoadingView = isLoading.asObservable()
        isSubmitButtonEnabled = Observable.combineLatest(
            email.asObservable(), password.asObservable()
        ) { email, password in
            let vm = JRValidationManager.self
            let isValidEmail = vm.didValidate(email, as: .email)
            let isValidPassword = vm.didValidate(password, as: .password)
            return isValidEmail && isValidPassword
        }
        emailTextColor = email.asObservable().map { email in
            let c = JRAuthenticationCredentialsView.Constant.anyTextField.self
            if JRValidationManager.didValidate(email, as: .email) {
                return c.validTextColor
            }
            return c.textColor
        }
        passwordTextColor = password.asObservable().map { password in
            let c = JRAuthenticationCredentialsView.Constant.anyTextField.self
            if JRValidationManager.didValidate(password, as: .password) {
                return c .validTextColor
            }
            return c.textColor
        }
        submitButtonTitle = submitButtonAction.asObservable().map { action in
            let c = JRAuthenticationCredentialsView.Constant.submitButton.self
            if action == .signIn {
                return c.signInTitle
            }
            return c.signUpTitle
        }
        signInUpButtonTitle = submitButtonAction.asObservable().map { action in
            let c = JRUserSignInUpView.Constant.SignInUpButton.self
            if action == .signIn {
                return c.signInTitle
            }
            return c.signUpTitle
        }
        super.init()
    }
    
    // MARK: - Exposed Functions
    
    func userDidTapView() {
        // TODO: Dismiss keyboard.
    }
    
    func userDidSetEmail(to e: String) {
        email.value = e
    }
    
    func userDidSetPassword(to p: String) {
        password.value = p
    }
    
    func userDidTapSubmitButton() {
        submitValidatedCredentials()
    }
    
    func userDidTapSignInUpButton() {
        submitButtonAction.value.toggle()
    }
    
    // MARK: - Private Functions
    
    private func submitValidatedCredentials() {
        let vm = JRValidationManager.self
        guard vm.didValidate(email.value, as: .email) && vm.didValidate(password.value, as: .password) else {
            return
        }
        beforeAuthenticationRequest()
        if submitButtonAction.value == .signIn {
            attemptSigningUpUser()
        } else {
            attemptSigningUpUser()
        }
    }
    
    private func attemptSigningInUser() {
        JRAuthenticationManager.attemptSigningIn(email: email.value, password: password.value) { [weak self] response in
            guard let weak = self else { return }
            switch response {
            case let .success(user):
                print(user)
            case let .failure(error):
                print(error)
            }
            weak.afterAuthenticationResponse()
        }
    }
    
    private func attemptSigningUpUser() {
        JRAuthenticationManager.attemptSigningUp(email: email.value, password: password.value) { [weak self] response in
            guard let weak = self else { return }
            switch response {
            case let .success(user):
                print(user)
            case let .failure(error):
                print(error)
            }
            weak.afterAuthenticationResponse()
        }
    }
    
    private func beforeAuthenticationRequest() {
        isLoading.value = true
    }
    
    private func afterAuthenticationResponse() {
        isLoading.value = false
    }
    
    // MARK: - Private Types
    
    private enum SubmitButtonAction: Int {
        case signIn, signUp
        mutating func toggle() {
            self = self == .signIn ? .signUp : .signIn
        }
    }

}
