//
//  JRFeedViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/23/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRFeedViewController: JRInstantiatedViewBindingController<JRFeedViewModel, JRFeedView> {
    
    // MARK: - Exposed Properties
    
    override var preferredNavigationBarTitle: String? {
        return Constant.NavigationBar.title
    }
    
    override var preferredTabBarTitle: String? {
        return Constant.TabBar.title
    }
    
    override var preferredTabBarImage: UIImage? {
        return Constant.TabBar.image
    }
    
    // MARK: - Private Constants
    
    private struct Constant {
        struct NavigationBar {
            static let title = "My Feed".localizedCapitalized
        }
        struct TabBar {
            static let title = "Feed".localizedCapitalized
            static let image = #imageLiteral(resourceName: "feed_tab")
        }
    }
    
}
