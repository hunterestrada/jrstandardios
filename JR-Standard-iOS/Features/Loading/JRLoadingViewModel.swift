//
//  JRLoadingViewModel.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/29/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RxSwift

final class JRLoadingViewModel: JRViewModel {
    
    // MARK: - Exposed Properties
    
    let titleLabelText: Observable<String>
    
    // MARK: - Exposed Functions
    
    init(title: String = JRLoadingView.Constant.TitleLabel.text) {
        titleLabelText = Observable.just(title)
        super.init()
    }
    
}
