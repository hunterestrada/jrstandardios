//
//  JRLoadingViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/29/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RxCocoa
import RxSwift

import UIKit

final class JRLoadingViewController: JRInstantiatedViewBindingController<JRLoadingViewModel, JRLoadingView> {
    
    // MARK: - Exposed Functions
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        v.customFadeIn(using: .fast)
    }
    
    // MARK: Lifecycle
    
    override func bind() {
        super.bind()
        guard let vm = viewModel else { return }
        // MARK: Take
        vm.titleLabelText.bind(
            to: v.titleLabel.rx.text
        ).disposed(by: disposer)
    }
    
}
