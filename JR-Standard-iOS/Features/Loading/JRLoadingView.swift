//
//  JRLoadingView.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 6/29/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

final class JRLoadingView: JRView {
    
    // MARK: - Exposed Properties
    
    @IBOutlet var visualEffectView: UIVisualEffectView!
    
    @IBOutlet var titleLabel: UILabel! {
        didSet {
            titleLabel.text = Constant.TitleLabel.text
        }
    }
    
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView! {
        didSet {
            activityIndicatorView.startAnimating()
        }
    }
    
    // MARK: - Exposed Functions
    
    func customFadeIn(using duration: JRAnimationDuration) {
        titleLabel.fadeIn(using: duration)
        activityIndicatorView.fadeIn(using: duration)
        duration.animate { [weak self] in
            guard let weak = self else { return }
            weak.visualEffectView.effect = UIBlurEffect(style: .light)
        }
    }
    
    func customFadeOut(using duration: JRAnimationDuration) {
        titleLabel.fadeOut(using: duration)
        activityIndicatorView.fadeOut(using: duration)
        duration.animate { [weak self] in
            guard let weak = self else { return }
            weak.visualEffectView.effect = nil
        }
    }
    
    // MARK: - Constants
    
    struct Constant {
        struct TitleLabel {
            static let text = "Loading".localizedCapitalized
        }
    }
    
}
