//
//  JROwningView.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit
import PureLayout

class JROwningView: JRView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let owner = classForCoder as? JRViewable.Type {
            if let view = owner.instantiatedView(owner: self, options: nil) {
                addSubview(view)
                view.autoPinEdgesToSuperviewEdges()
            }
        }
    }
    
}
