//
//  JRInstantiatedViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

class JRInstantiatedViewController<View>: JRViewController where View: JRView {
    
    var v: View!
    
    override func loadView() {
        if let instantiated = View.instantiated() {
            view = instantiated
            v = instantiated
        } else {
            super.loadView()
        }
    }
    
}
