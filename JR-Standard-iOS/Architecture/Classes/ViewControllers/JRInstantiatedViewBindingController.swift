//
//  JRInstantiatedViewBindingController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit
import RxSwift

class JRInstantiatedViewBindingController<ViewModel, View>: JRInstantiatedViewController<View>, JRViewBinding where ViewModel: JRViewModeling, View: JRView  {
    
    // MARK: - Exposed Properties
    
    var viewModel: ViewModel?
    
    let disposer = DisposeBag()
    
    // MARK: - Exposed Functions
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }
    
    func bind() {
        // Requires custom implementation by subclassing view controller.
    }
    
}

