//
//  JRInstantiatedTableSuperViewBindingController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

class JRInstantiatedTableSuperViewBindingController<ViewModel, View>: JRInstantiatedViewBindingController<ViewModel, View>, UITableViewDataSource, UITableViewDelegate where ViewModel: JRTableSuperViewModel, View: JRView {
    
    // MARK: - Exposed Functions
    
    override func bind() {
        super.bind()
        guard let vm = viewModel else { return }
        guard let tv: UITableView = v.closest(.subview) else { return }
        tv.rx.setDelegate(self).disposed(by: disposer)
        tv.rx.setDataSource(self).disposed(by: disposer)
        tv.rx.itemSelected.bind { indexPath in
            vm.userDidTapItem(at: indexPath)
        }.disposed(by: disposer)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let vm = viewModel else {
            return JRNumber.zero.int
        }
        return vm.tableViewData.value.sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let vm = viewModel else {
            return JRNumber.zero.int
        }
        return vm.tableViewData.value.rowCount(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
        
}
