//
//  JRViewController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

class JRViewController: UIViewController, JRViewControlling, JRNavigationBarTitling, JRTabBarTitling, JRBackButtonTitling, JRNavigationBarButtonItemProviding {
    
    // MARK: - Exposed Properties
    
    // MARK: Dismissing Logic
    
    /// Allows view controller to dismiss itself according to its presenting view controller's logic.
    var dismissHandler: (() -> Void)?
    
    // MARK: Navigation Bar
    
    var prefersNavigationBarHidden: Bool {
        return false
    }
    
    var preferredNavigationBarStyle: UIBarStyle? {
        return nil
    }
    
    var preferredNavigationBarTitle: String? {
        return nil
    }
    
    var preferredBackBarButtonItemTitle: String? {
        return nil
    }
    
    var preferredNavigationBarButtonItemsLeft: [UIBarButtonItem] {
        return []
    }
    
    var preferredNavigationBarButtonItemsRight: [UIBarButtonItem] {
        return []
    }
    
    // MARK: Tab Bar
    
    var preferredTabBarTitle: String? {
        return nil
    }
    
    var preferredTabBarImage: UIImage? {
        return nil
    }
    
    // MARK: - Exposed Functions
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = preferredNavigationBarTitle
        navigationItem.setLeftBarButtonItems(preferredNavigationBarButtonItemsLeft, animated: false)
        navigationItem.setRightBarButtonItems(preferredNavigationBarButtonItemsRight, animated: false)
        tabBarItem.title = preferredTabBarTitle
        tabBarItem.image = preferredTabBarImage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let nc = navigationController {
            nc.setNavigationBarHidden(prefersNavigationBarHidden, animated: animated)
            if let s = preferredNavigationBarStyle {
                nc.navigationBar.barStyle = s
            }
        }
        if let kp = self as? JRKeyboardPresenting {
            kp.observeKeyboardNotifications()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let kp = self as? JRKeyboardPresenting {
            kp.ignoreKeyboardNotifications()
        }
    }
    
    // MARK: JRKeyboardPresenting
    
    func observeKeyboardNotifications() {
        guard let keyboardPresenter = self as? JRKeyboardPresenting else {
            return
        }
        keyboardPresenter.keyboardNotificationNames.forEach { name in
            JRNotificationManager.add(self, with: #selector(receiveKeyboard(_:)), for: name)
        }
    }
    
    func ignoreKeyboardNotifications() {
        guard let keyboardPresenter = self as? JRKeyboardPresenting else {
            return
        }
        keyboardPresenter.keyboardNotificationNames.forEach { name in
            JRNotificationManager.remove(self, for: name)
        }
    }
    
    func receiveKeyboard(_ notification: Notification) {
        guard let keyboardPresenter = self as? JRKeyboardPresenting else {
            return
        }
        guard let keyboard = JRKeyboard(keyboardNotification: notification) else {
            return
        }
        switch notification.name {
        case Notification.Name.UIKeyboardWillShow:
            keyboardPresenter.keyboardWillShow(keyboard)
        case Notification.Name.UIKeyboardWillHide:
            keyboardPresenter.keyboardWillHide(keyboard)
        case Notification.Name.UIKeyboardDidShow:
            keyboardPresenter.keyboardDidShow(keyboard)
        case Notification.Name.UIKeyboardDidHide:
            keyboardPresenter.keyboardDidHide(keyboard)
        default:
            return
        }
    }
    
}
