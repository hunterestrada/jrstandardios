//
//  JRKeyboard.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

struct JRKeyboard {
    
    let height: CGFloat
    let animationDuration: TimeInterval
    let animationOptions: UIViewAnimationOptions
    
    init(height: CGFloat, animationDuration: TimeInterval, animationOptions: UIViewAnimationOptions) {
        self.height = height
        self.animationDuration = animationDuration
        self.animationOptions = animationOptions
    }
    
    /**
     Initializes with UIKeyboardWillShow or UIKeyboardWillHide notifications.
     */
    init?(keyboardNotification notification: Notification) {
        guard let information = notification.userInfo, notification.name == Notification.Name.UIKeyboardWillHide || notification.name == Notification.Name.UIKeyboardWillShow || notification.name == Notification.Name.UIKeyboardDidHide || notification.name == Notification.Name.UIKeyboardDidShow else {
            return nil
        }
        guard let height = (information[UIKeyboardFrameEndUserInfoKey] as? CGRect)?.height else {
            return nil
        }
        guard let animationDuration = information[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval else {
            return nil
        }
        guard let animationOptionsRawValue = information[UIKeyboardAnimationCurveUserInfoKey] as? UInt else {
            return nil
        }
        self.height = height
        self.animationDuration = animationDuration
        self.animationOptions = UIViewAnimationOptions(rawValue: animationOptionsRawValue)
    }
    
}
