//
//  JRTableSuperViewModel.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import RxSwift

class JRTableSuperViewModel: JRViewModel {
    
    // MARK: - Exposed Properties
    
    let tableViewData = Variable([[JRDataPresenter<JRDataModel>]]())
    
    // MARK: - Exposed Functions
    
    func dataPresenter(at indexPath: IndexPath) -> JRDataPresenter<JRDataModel>? {
        return tableViewData.value[safe: indexPath]
    }
    
    // MARK: Actions
    
    func userDidTapItem(at indexPath: IndexPath) {
        
    }
    
}
