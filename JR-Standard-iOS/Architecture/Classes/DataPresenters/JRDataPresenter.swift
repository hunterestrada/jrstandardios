//
//  JRDataPresenter.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

class JRDataPresenter<DataModel>: JRDataPresenting where DataModel: JRDataModeling {
    
    let rawDataModel: DataModel
    
    required init(of model: DataModel) {
        rawDataModel = model
    }
    
}
