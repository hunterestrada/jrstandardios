//
//  JRNavigationController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

class JRNavigationController: UINavigationController, JRNavigationBarControlling {
    
    // MARK: - Exposed Properties
    
    var dismissHandler: (() -> Void)?
    
    var preferredTabBarTitle: String? {
        return (topViewController as? JRTabBarTitling)?.preferredTabBarTitle
    }
    
    // MARK: - Exposed Functions
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if let vc = topViewController {
            let t = (vc as? JRBackButtonTitling)?.preferredBackBarButtonItemTitle ?? String.empty
            vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: t, style: .plain, target: nil, action: nil)
        }
        super.pushViewController(viewController, animated: animated)
    }
    
}
