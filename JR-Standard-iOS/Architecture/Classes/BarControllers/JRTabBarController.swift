//
//  JRTabBarController.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

class JRTabBarController: UITabBarController, UITabBarControllerDelegate, JRTabBarControlling, JRNavigationBarButtonItemProviding {
    
    // MARK: - Exposed Properties
    
    var dismissHandler: (() -> Void)?
    
    var preferredNavigationBarTitle: String? {
        return (selectedViewController as? JRNavigationBarTitling)?.preferredNavigationBarTitle
    }
    
    var preferredNavigationBarButtonItemsLeft: [UIBarButtonItem] {
        return (selectedViewController as? JRNavigationBarButtonItemProviding)?.preferredNavigationBarButtonItemsLeft ?? []
    }
    
    var preferredNavigationBarButtonItemsRight: [UIBarButtonItem] {
        return (selectedViewController as? JRNavigationBarButtonItemProviding)?.preferredNavigationBarButtonItemsRight ?? []
    }
    
    var preferredBackBarButtonItemTitle: String? {
        return nil
    }
    
    // MARK: - Exposed Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    
    override func setViewControllers(_ viewControllers: [UIViewController]?, animated: Bool) {
        super.setViewControllers(viewControllers, animated: animated)
        updateNavigationItem()
        viewControllers?.forEach { vc in
            vc.loadViewIfNeeded()
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        updateNavigationItem()
    }
    
    // MARK: - Private Functions
    
    private func updateNavigationItem() {
        navigationItem.title = preferredNavigationBarTitle
        navigationItem.setLeftBarButtonItems(preferredNavigationBarButtonItemsLeft, animated: false)
        navigationItem.setRightBarButtonItems(preferredNavigationBarButtonItemsRight, animated: false)
    }

}
