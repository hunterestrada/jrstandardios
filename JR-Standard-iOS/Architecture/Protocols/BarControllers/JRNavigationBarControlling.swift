//
//  JRNavigationBarControlling.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

protocol JRNavigationBarControlling: JRBarControlling, JRTabBarTitling {
    
}
