//
//  JRViewable.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

/**
 A protocol for any view to conform.
 */
protocol JRViewable: class, JRBundlable, JRIdentifiable {
    
}

// MARK: - Default Implementation

extension JRViewable {
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: bundle)
    }
    
    static func instantiated(owner: Any? = nil, options: [AnyHashable : Any]? = nil) -> Self? {
        return instantiatedView(owner: owner, options: options) as? Self
    }
    
    static func instantiatedView(owner: Any? = nil, options: [AnyHashable : Any]? = nil) -> UIView? {
        let i = nib.instantiate(withOwner: owner, options: options)
        return i.first as? UIView
    }
    
}
