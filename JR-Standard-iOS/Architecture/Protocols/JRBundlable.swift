//
//  JRBundlable.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

protocol JRBundlable: class {
        
}

// MARK: - Default Implementation

extension JRBundlable {
    
    static var bundle: Bundle {
        return Bundle(for: self)
    }
    
}
