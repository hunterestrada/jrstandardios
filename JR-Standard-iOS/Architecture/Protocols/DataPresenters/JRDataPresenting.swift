//
//  JRDataPresenting.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

/**
 A protocol for any data presenter to conform. Conforming data presenters serve as a wrapper interface for the underlying data model and allow them to be passed by reference. Conformists should rely heavily on computed properties to produce localized strings, UIImages, etc... Unlike data models, data presenters are not restricted from importing frameworks like UIKit.
 */
public protocol JRDataPresenting: class {
    
    // MARK: Associated Types
    
    /**
     A generic data model.
     */
    associatedtype DataModel: JRDataModeling
    
    // MARK: - Statically Exposed Properties
    
    /**
     A property for the underlying data model. Views should never interact with the raw model, and view controllers should avoid interacting with the raw model unless absolutely necessary.
     */
    var rawDataModel: DataModel { get }
    
    // MARK: - Exposed Functions
    
    /**
     An initializer to wrap the underlying data model.
     */
    init(of dataModel: DataModel)
    
}
