//
//  JRViewControlling.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

/**
 A protocol for any view controller to conform if it doesn't have a corresponding view model.
 */
protocol JRViewControlling: class, JRBundlable, JRIdentifiable {
    
    // MARK: - Exposed Properties
    
    var dismissHandler: (() -> Void)? { get set }
        
    // MARK: - Statically Exposed Properties
    
    /**
     A property to uniquely identify a view controller within a Storyboard or Xib file.
     */
    static var identifier: String { get }
        
}
