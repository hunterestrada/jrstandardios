//
//  JRViewBinding.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

/**
 A protocol for any view controller to conform if it has a corresponding view model.
 */
protocol JRViewBinding: JRViewControlling {
    
    // MARK: - Associated Types
    
    associatedtype ViewModel: JRViewModeling
    
    // MARK: - Exposed Properties
    
    /**
     A provided property that manages the variable state of the view controller and exposes properties related to that state. For customizability, conforming view controllers should always be given their view models by other view controllers. However, conforming view controllers can decide when to bind to their view models within their own lifecycles.
     */
    var viewModel: ViewModel? { get set }
    
    // MARK: - Exposed Functions
    
    /**
     A function in which the conforming view controller binds to its provided view model. The conforming view controller should use a reactive library to subscribe to state changes of the view model.
     */
    func bind()
    
}

