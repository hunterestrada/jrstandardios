//
//  JRKeyboardPresenting.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/13/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import Foundation

protocol JRKeyboardPresenting: JRViewControlling {
    
    var keyboardNotificationNames: [Notification.Name] { get }
    
    func observeKeyboardNotifications()
    func ignoreKeyboardNotifications()
    
    func receiveKeyboard(_ notification: Notification)
    
    func keyboardWillShow(_ keyboard: JRKeyboard)
    func keyboardWillHide(_ keyboard: JRKeyboard)
    func keyboardDidShow(_ keyboard: JRKeyboard)
    func keyboardDidHide(_ keyboard: JRKeyboard)
    
}

// MARK: - Default Implementation

extension JRKeyboardPresenting {
    
    var keyboardNotificationNames: [Notification.Name] {
        return [
            .UIKeyboardWillShow, .UIKeyboardWillHide,
            .UIKeyboardDidShow, .UIKeyboardDidHide
        ]
    }
    
    func keyboardWillShow(_ keyboard: JRKeyboard) {
        
    }
    
    func keyboardWillHide(_ keyboard: JRKeyboard) {
        
    }
    
    func keyboardDidShow(_ keyboard: JRKeyboard) {
        
    }
    
    func keyboardDidHide(_ keyboard: JRKeyboard) {
        
    }
    
}
