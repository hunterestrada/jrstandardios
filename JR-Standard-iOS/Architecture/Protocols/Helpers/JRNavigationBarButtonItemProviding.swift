//
//  JRNavigationBarButtonItemProviding.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/23/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import UIKit

protocol JRNavigationBarButtonItemProviding {
    
    var preferredNavigationBarButtonItemsLeft: [UIBarButtonItem] {
        get
    }
    
    var preferredNavigationBarButtonItemsRight: [UIBarButtonItem] {
        get
    }
    
}
