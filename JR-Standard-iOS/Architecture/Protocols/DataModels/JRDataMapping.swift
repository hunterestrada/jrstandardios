//
//  JRDataMapping.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

import SwiftyJSON

/**
 A protocol to which data models should conform if they are represented within network responses and network requests.
 */
public protocol JRDataMapping: JRDataModeling {
    
    // MARK: - Exposed Properties
    
    /**
     A property that converts conforming data models to a payload that can be represented within network requests.
     */
    func toPayload() -> JSON
    
    // MARK: - Statically Exposed Functions
    
    /**
     A property that converts the payload of a network request to a conforming data model if possible.
     */
    static func fromPayload(_ payload: JSON) -> Self?
    
}

