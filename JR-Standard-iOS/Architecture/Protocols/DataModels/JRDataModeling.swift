//
//  JRDataModeling.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

/**
 A protocol to which data models should conform if they are not represented within network responses and network requests. Data models should always have a corresponding data presenter to interface with views and view controllers.
 */
public protocol JRDataModeling {
    
}
