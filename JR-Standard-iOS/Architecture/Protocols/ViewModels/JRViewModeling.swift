//
//  JRViewModeling.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

/**
 A protocol for any view model to conform. Conforming view models should manage the variable state of their corresponding view controller and expose properties relating to that state. For testability, conforming view models should never import UIKit.
 */
public protocol JRViewModeling: class {
    
}
