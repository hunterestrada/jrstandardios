//
//  JRIdentifiable.swift
//  JR-Standard-iOS
//
//  Created by Hunter Estrada on 7/12/17.
//  Copyright © 2017 Jackrabbit Mobile. All rights reserved.
//

public protocol JRIdentifiable {
    
    static var identifier: String { get }
    
}

// MARK: - Default Implementation

public extension JRIdentifiable {
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
