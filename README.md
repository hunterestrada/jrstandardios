# JR-Standard-iOS
JR-Standard-iOS is a foundational project that's built to scale. It provides generic code to standardize the underlying implementation of any iOS application. It focuses on structuring the hierarchy from the underlying models to the presented views as well as providing core functionality at each layer. By inheriting from its classes or conforming to its protocols, specialized types adhere to common conventions throughout a project. By understanding these conventions, developers can build functionality in a highly consistent fashion, rapidly onboard onto existing projects, and efficiently review code written by other developers. In short, JR-Standard-iOS aims to provide a solid foundation for maintainable projects.

# Project Structure

# Language: Swift

JR-Standard-iOS uses Swift to access generics, optionals, closures, protocols, structs, and strict typing. Ensure that you're following best practices by conforming to Apple's guidelines: <https://swift.org/documentation/api-design-guidelines/>. Put briefly, always safely unwrap optionals, write functions that read like sentences, name properties descriptively, and avoid magic values.

# Paradigms
### Protocol-Oriented Programming
* Prioritize default implementations over inherited implementations to build modularized code sites that do not contain unnecessary functionality
* Prioritize value types (structs) over reference types (classes) to gain the benefits of value semantics with respect to immutability

### Functional-Reactive Programming
* Minimize variable state as much as possible throughout the application
* Localize variable state to the narrowest possible scope
* Isolate variable state of views within view models and bind the actual views to that variable state
* Prioritize closures to decouple code sites over delegates
* Prioritize higher-order functions (e.g., map, filter, and reduce) over more imperative iteration styles
* Avoid singleton properties

# Dependencies: Cocoapods
See the Podfile for more information about dependencies.

# Architecture: MVVM
JR-Standard-iOS is built with the model-view-view-model (MVVM) architecture. To maintain consistency across MVVM-related sites, ensure that every related type conforms to the following protocols:

### Data Model
* **JRDataModeling** 
	* Base protocol to which every model should conform
	* Use default extensions to facilitate database transactions
* **JRDataMapping**
	* Networking-related protocol to which every model should conform if it must be converted to/from JSON
	* Use default extensions to facilitate network requests

### Data Presenter

* **JRDataPresenting**
	* Wraps around a model to interface with any view
	* Relies heavily on computed properties that are based on the underlying model
	* Provides images and localized strings
	* UIKit can be imported without any downside

### View
The following sites relate to UIView and UIViewController. To allow for generic code, only use Interface Builder for creating views that can be loaded into view controllers. Avoid storyboards and XIBs that contain instances of view controllers.

* **JRView**
	* Base class to which every main view should inherit
	* Use default extensions to reduce boilerplate code that deals with instantiation
* **JRViewController**
	* Base class to which every view controller should conform
	* Use default extensions to reduce boilerplate code that deals with instantiation
* **JRInstantiatedViewController**
	* Base class to which every view controller should conform if it has an instantiated view in a XIB file
	* Use 'v' instead of 'view' to access the actual view property.
	* Use default extensions to reduce boilerplate code that deals with instantiation
* **JRInstantiatedViewBindingController**
	* Base class to which every view controller should inherit if it has an instantiated view in a XIB file and a related view model
	* Controls when 'bind()' is called within their lifecycle
	* No view controller should initialize its own view model if it inherits from this class

### View Model
The following sites should focus on managing variable state of their related views but should never, under any circumstance, import UIKit. Any UI-related testing should focus on these sites and assume that bindings will ensure proper presentation of the actual views.

* **JRViewModel**
	* Base class from which every view model should inherit
	* Only view controllers should have a view model
	* View controllers should initialize the view model of another view controller to promote separation of concerns

# Managers

Beyond the standard MVVM-related components, JR-Standard-iOS introduces the following managers to promote reusablility and modularity. These sites collaborate with each other to handle logic that relates to many feature sets. They are globally accessible yet intentionally stateless.

* **JRApplicationManager**
	* Manages any lifecycle-dependent managers
	* Manages initial appearance of window (including user-onboarding)
	* Manages loading of necessary data before allowing user interaction
* **JRAppearanceManager**
	* Manages any UI-related constants that are used throughout the app
* **JRAuthenticationManager**
	* Manages authenticated user with continuously refreshed tokens
	* Manages sign up/in networking flow
	* Manages storing of user's credentials into keychain 
* **JRNetworkManager**
	* Manages networking activity throughout the application
	* Manages endpoints, requests, and headers
* **JRDatabaseManager**
	* Manages database transactions of any persistable model
* **JRLocationManager**
	* Manages GPS-related code throughout app
* **JRValidationManager**
	* Manages validation of user's credentials locally
	* Manages validation of url-related strings
* **JRNotificationManager**
	* Manages posting of general notifications
	* Manages adding and removing of responders
* **JRKeychainManager**
	* Manages keychain transactions of any user-related information that's sensitive (e.g., passwords and authentication tokens)